package com.tatsumideviluke.top10;

/**
 * Created by Brian on 12/27/15.
 */
public class Application {
    private String title;
    private String description;
    private String pubdate;
    private String link;

    public String getName() {
        return title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPubdate() {
        return pubdate;
    }

    public void setPubdate(String pubdate) {
        this.pubdate = pubdate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return  "Title: "+ getTitle() + "\n\n" +
                "Link: "+ getLink() +"\n\n" +
                "Description: " + getDescription() + "\n\n" +
                "Date: " + getPubdate() + "\n";
    }
}
